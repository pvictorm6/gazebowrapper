#ifndef _FAKE_ROBOT_GAZEBO_
#define _FAKE_ROBOT_GAZEBO_

#include "gazebowrapper/ReqFeedback.h"
#include "gazebowrapper/CommandTorques.h"
#include <ros/ros.h>

class GzRobot
{
public:
    GzRobot(ros::NodeHandlePtr &nh);
    void spinOnce();
    void getFeedback(double *feedback);
    void getContacts(double *contacts);
    void setCommand(double *command);
private:
    double* actual_feedback;
    double* actual_contacts;
    //ros::ServiceClient feedback_client;
    //ros::ServiceClient command_client;
    gazebowrapper::ReqFeedback feedback;
    gazebowrapper::CommandTorques torques;
    ros::ServiceClientPtr feedback_client;
    ros::ServiceClientPtr command_client;

};


#endif
