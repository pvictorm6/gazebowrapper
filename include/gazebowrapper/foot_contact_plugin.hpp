#ifndef _GAZEBO_FOOTCONTACT_PLUGIN_HH_
#define _GAZEBO_FOOTCONTACT_PLUGIN_HH_

#include <string>

#include <gazebo/gazebo.hh>
#include <gazebo/sensors/sensors.hh>
#include <eigen3/Eigen/Eigen>
#include <vector>

class contactHandler
{
public:
    std::string contactName;
    bool isContact;
    Eigen::Vector3d force;
    Eigen::Vector3d torque;
    Eigen::Vector3d position;
    Eigen::Vector3d normal;
};

typedef boost::shared_ptr<contactHandler> contactHandlerPtr;

namespace gazebo
{
    class FootContactPlugin : public SensorPlugin
    {
    public:
        FootContactPlugin();

        virtual ~FootContactPlugin();

        virtual void Load(sensors::SensorPtr _sensor, sdf::ElementPtr _sdf);
    private:
        virtual void OnUpdate();

        sensors::ContactSensorPtr parentSensor;
        event::ConnectionPtr updateConnection;

        int nContactSensors;
        std::vector<contactHandlerPtr> modelContacts;
    };
}

#endif
