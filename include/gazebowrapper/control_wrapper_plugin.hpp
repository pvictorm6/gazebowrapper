#ifndef _GAZEBO_CONTROL_WRAPPER_PLUGIN_HH_
#define _GAZEBO_CONTROL_WRAPPER_PLUGIN_HH_

#include <gazebo/gazebo.hh>
#include <gazebo/sensors/sensors.hh>
#include <eigen3/Eigen/Eigen>
#include <gazebo/common/common.hh>
#include <gazebo/physics/physics.hh>
#include <boost/bind.hpp>
#include <stdio.h>

#include <ros/ros.h>
#include "gazebowrapper/ReqFeedback.h"
#include "gazebowrapper/CommandTorques.h"

using namespace Eigen;

class contactHandler
{
public:
    std::string contactName;
    bool isContact;
    Eigen::Vector3d force;
    Eigen::Vector3d torque;
    Eigen::Vector3d position;
    Eigen::Vector3d normal;

    std::string topicName;
    gazebo::transport::SubscriberPtr subscriber;

    void contactUpdate(ConstContactsPtr &_msg)
    {
        if(_msg->contact_size() > 0)
        {
            isContact = true;
        }else{
            isContact = false;
        }
    }
};

class jointHandler
{
public:
    std::string jointName;
    gazebo::physics::JointPtr jointPtr;
    double actualAngle;
    double actualVelocity;
    double actualTorque;
    double controlSignal;

    jointHandler()
    {
        actualAngle = 0;
        actualVelocity = 0;
        controlSignal = 0;
    }

    void getStates()
    {
        actualAngle = jointPtr->GetAngle(0).Radian();
        actualVelocity = jointPtr->GetVelocity(0);
        actualTorque = jointPtr->GetForce(0);
    }

    void setPosition(double pos)
    {
        jointPtr->SetPosition(0,pos);
    }

    void setVelocity(double vel)
    {
        jointPtr->SetVelocity(0,vel);
    }

    void setEffort()
    {
        jointPtr->SetForce(0, controlSignal);
    }
};

typedef boost::shared_ptr<contactHandler> contactHandlerPtr;
typedef boost::shared_ptr<jointHandler> jointHandlerPtr;
typedef boost::shared_ptr<ros::ServiceServer> ServiceServerPtr;

namespace gazebo
{
    class ControlWrapperPlugin : public ModelPlugin
    {
    public:
        ControlWrapperPlugin();

        ~ControlWrapperPlugin();

        virtual void Load(physics::ModelPtr _parent, sdf::ElementPtr _sdf);

        virtual void OnUpdate(const common::UpdateInfo &);

        VectorXd getFeedback();

        void setControlSignals(VectorXd U_signal, int flag_reference);

        VectorXd getContactState();

        bool reqFeedback(gazebowrapper::ReqFeedbackRequest &req, gazebowrapper::ReqFeedbackResponse &res);
        bool commTorques(gazebowrapper::CommandTorquesRequest &req, gazebowrapper::CommandTorquesResponse &res);
    private:

        physics::ModelPtr model;
        int nContactSensors;
        int nJoints;
        std::vector<contactHandlerPtr> modelContacts;
        transport::NodePtr wrapperNode;
        event::ConnectionPtr updateConnection;

        physics::JointPtr ac_rAnkle;
        physics::JointPtr ac_rKnee;
        physics::JointPtr ac_rHip;
        physics::JointPtr ac_lHip;
        physics::JointPtr ac_lKnee;
        physics::JointPtr ac_lAnkle;

        std::vector<jointHandlerPtr> modelJoints;

        void contactRUpdate(ConstContactsPtr &_msg);
        void contactLUpdate(ConstContactsPtr &_msg);
        gazebo::transport::SubscriberPtr subs;

        ros::NodeHandlePtr ros_node;
        ros::ServiceServer ros_feedback_service;
        ros::ServiceServer ros_command_service;

        int argc;
        char **argv;


    };
}

#endif
