# README #

The Gazebo Simulator for Humanoid Robots is a package designed to abstract the simulation code from the controller development, the library fake_robot enable to abstract the simulation and interface it as it were real hardware.

### Requisites ###

* Ubuntu 14.04
* ROS Jade (Should be fine from ROS Hydro)
* Gazebo 5

### Installing ###
Open a terminal in your ros workspace, then:

    cd src
    git clone git@bitbucket.org:pvictorm6/gazebowrapper.git
    cd ../
    catkin_make

### Abstractor Class Usage ###
The class provides an example code, called wrapper_test.cpp, which uses a PD control in order to keep the robot standing. From the code, the important snippets are explained:

    #include "gazebowrapper/fake_robot.hpp

This is the header which will allow to call the robot simulation abstractor, this library provides the class: GzRobot.


GzRobot expects a ros::NodleHandlePtr as argument, so make sure to use:

    ros::NodeHandlePtr nh;

Instead of:
   
    ros::NodeHandle nh;

Remember to initialize the pointer to a class:

    nh = ros::NodeHandlePtr(new ros::NodeHandle);


And then create the class: GzRobot simRobot(nh); 


simRobot has three methods, 

    simRobot.spinOnce(): Reads all the sensors in the simulated robot.
    simRobot.getFeedback(double*): Populates the argument with [Position, Velocity, Torque] of each Link.
    simRobot.getContacts(int*): Populated the argument with the state of the contact sensors. 
    simRobot.setCommand(double): Commands the torque to the simulated Robot.


### Do you want to contribute? ###
Please, go through the ROS and Gazebo Tutorials and contact me, there is open problems with this package, some of them are:

* Pick an initial condition
* GUI to switch Controllers in RT
* Support for More Contacts with the environment.
* etc...