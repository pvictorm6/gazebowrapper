#include "wrapper_test.hpp"
#include <ros/ros.h>

#include "gazebowrapper/ReqFeedback.h"
#include "gazebowrapper/CommandTorques.h"
#include "gazebowrapper/fake_robot.hpp"

#include <stdlib.h>

int main(int argc, char **argv)
{
    ros::init(argc, argv, "wrapper_test");
    ros::NodeHandlePtr nh;
    double simFeedback[18];
    double simCommand[6];

    gazebowrapper::ReqFeedback feedback;
    gazebowrapper::CommandTorques torques;
    nh = ros::NodeHandlePtr(new ros::NodeHandle);

    GzRobot simRobot(nh);

    ros::Rate rate(300);
    while(ros::ok())
    {
        simRobot.spinOnce();
        simRobot.getFeedback(simFeedback);

        std::cout<<"Feedback sample: "<<simFeedback[0]<<std::endl;

        // Testing PD control to keep static position
        for(int i = 0; i < 6 ; ++i)
        {
            simCommand[i] = -600 * simFeedback[3*i + 0] - 10 * simFeedback[3*i + 1];
        }

        simRobot.setCommand(simCommand);

        ros::spinOnce();
        rate.sleep();
    }

    return 0;
}
