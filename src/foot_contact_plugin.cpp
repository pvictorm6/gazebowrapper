#include "foot_contact_plugin.hpp"
#include <eigen3/Eigen/Eigen>

using namespace gazebo;
GZ_REGISTER_SENSOR_PLUGIN(FootContactPlugin);

FootContactPlugin::FootContactPlugin() : SensorPlugin()
{
    nContactSensors = 2;

    modelContacts.resize(nContactSensors);

    modelContacts[0] = contactHandlerPtr(new contactHandler);
    modelContacts[1] = contactHandlerPtr(new contactHandler);

    modelContacts[0]->contactName = std::string("RFeetContact");
    modelContacts[1]->contactName = std::string("LFeetContact");

    modelContacts[0]->isContact = false;
    modelContacts[1]->isContact = false;

    // TODO: If using force we should include as well initialization methos for wrenches , normal directin and position as well


}

FootContactPlugin::~FootContactPlugin()
{

}

void FootContactPlugin::Load(sensors::SensorPtr _sensor, sdf::ElementPtr _sdf)
{
    this->parentSensor = boost::dynamic_pointer_cast<sensors::ContactSensor>(_sensor);

    if(!this->parentSensor){
        gzerr << "FootContactPlugin requieres a contact sensor. \n";
        return;
    }

    this->updateConnection = this->parentSensor->ConnectUpdated( boost::bind(&FootContactPlugin::OnUpdate, this) );

    this->parentSensor->SetActive(true);
}

void FootContactPlugin::OnUpdate()
{
 //std::cout<<"loaded!"<<std::endl;
}
