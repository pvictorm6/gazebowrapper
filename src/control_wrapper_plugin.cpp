#include "control_wrapper_plugin.hpp"

using namespace gazebo;
GZ_REGISTER_MODEL_PLUGIN(ControlWrapperPlugin);

ControlWrapperPlugin::~ControlWrapperPlugin()
{
    event::Events::DisconnectWorldUpdateBegin(this->updateConnection);
    wrapperNode->Fini();
    ros_node->shutdown();

    ros_node.reset();
}

ControlWrapperPlugin::ControlWrapperPlugin()
{

  std::cout<<"Loading wrapper plugin"<<std::endl;
  nContactSensors = 2;
  modelContacts.resize(nContactSensors);

  modelContacts[0] = contactHandlerPtr(new contactHandler);
  modelContacts[1] = contactHandlerPtr(new contactHandler);

  modelContacts[0]->contactName = std::string("RFeetContact");
  modelContacts[1]->contactName = std::string("LFeetContact");

  modelContacts[0]->topicName = "/gazebo/default/human_model/RFeet/RFeetContact";
  modelContacts[1]->topicName = "/gazebo/default/human_model/LFeet/LFeetContact";

  for(int i = 0; i < 2; ++i)
    {
      modelContacts[i]->isContact = false;
    }

  nJoints = 6;
  modelJoints.resize(nJoints);
  std::string Labels[] = {"RAnkle", "RKnee", "RHip", "LHip", "LKnee", "LAnkle"};

  for(int i = 0; i < nJoints; ++i)
  {
    modelJoints[i] = jointHandlerPtr(new jointHandler);
    modelJoints[i]->jointName = Labels[i];
  }


}


void ControlWrapperPlugin::Load(physics::ModelPtr _parent, sdf::ElementPtr _sdf)
{
  this->model = _parent;

  std::cout<<"Plugin Loaded and Model Obtained"<<std::endl;

  wrapperNode = transport::NodePtr(new transport::Node() );
  wrapperNode->Init();


  if(!ros::isInitialized())
  {

      int argc = 0;
      char **argv = NULL;
      ros::init(argc,argv,"gazebo_wrapper"); //,ros::init_options::NoSigintHandler|ros::init_options::AnonymousName);
  }

  ros_node = ros::NodeHandlePtr(new ros::NodeHandle());


  ros_feedback_service = ros_node->advertiseService("reqFeedback", &ControlWrapperPlugin::reqFeedback, this);

  ros_command_service = ros_node->advertiseService("commTorques", &ControlWrapperPlugin::commTorques, this);


  this->updateConnection = event::Events::ConnectWorldUpdateBegin( boost::bind(&ControlWrapperPlugin::OnUpdate, this, _1) );

  modelContacts[0]->subscriber = wrapperNode->Subscribe(modelContacts[0]->topicName, &ControlWrapperPlugin::contactRUpdate, this);
  modelContacts[1]->subscriber = wrapperNode->Subscribe(modelContacts[1]->topicName, &ControlWrapperPlugin::contactLUpdate, this);




  for(int i = 0; i < nJoints; ++i)
  {
      modelJoints[i]->jointPtr = this->model->GetJoint(modelJoints[i]->jointName);
  }


  /* Initial Condition (Optional) */
  modelJoints[0]->setVelocity(-0.1);
  modelJoints[1]->setVelocity(0.05);
  modelJoints[2]->setVelocity(-0.05);


}

void ControlWrapperPlugin::OnUpdate(const common::UpdateInfo &/* _info */)
{
    for(int i = 0; i < nJoints; ++i)
    {
        modelJoints[i]->getStates();
    }

    for(int i = 0; i < nJoints; ++i)
    {
        modelJoints[i]->setEffort();
    }

    ros::spinOnce();

}

VectorXd ControlWrapperPlugin::getFeedback()
{
    VectorXd feedback;
    for(int i = 0; i < nJoints; ++i)
    {
        feedback(i) = modelJoints[i]->actualAngle;
    }
    return feedback;
}

VectorXd ControlWrapperPlugin::getContactState()
{
    VectorXd contactState;
    for(int i = 0; i < nContactSensors; ++i)
    {
        contactState(i) = (modelContacts[i]->isContact == true ? 1 : 0);
    }
    return contactState;
}

void ControlWrapperPlugin::setControlSignals(VectorXd U_signal, int flag_reference=0)
{
    if(flag_reference == 1)
    {
        for(int i = 0; i < nJoints; ++i)
        {
            modelJoints[nJoints - i - 1]->controlSignal = U_signal(nJoints - i - 1);
        }
    }else{
        for(int i = 0; i < nJoints; ++i)
        {
            modelJoints[i]->controlSignal = U_signal(i);
        }
    }
}

void ControlWrapperPlugin::contactRUpdate(ConstContactsPtr &_msg)
{
    if(_msg->contact_size() > 0)
    {
        modelContacts[0]->isContact = true;
    }else
    {
        modelContacts[0]->isContact = false;
    }
}

void ControlWrapperPlugin::contactLUpdate(ConstContactsPtr &_msg)
{
    if(_msg->contact_size() > 0)
    {
        modelContacts[1]->isContact = true;

    }else
    {
        modelContacts[1]->isContact = false;
    }
}

bool ControlWrapperPlugin::reqFeedback(gazebowrapper::ReqFeedbackRequest &req, gazebowrapper::ReqFeedbackResponse &res)
{
    int flag = req.flag_reference;

    if(flag == 0)
    {
        res.sAnkle_pos = modelJoints[0]->actualAngle;
        res.sKnee_pos = modelJoints[1]->actualAngle;
        res.sHip_pos = modelJoints[2]->actualAngle;
        res.nsHip_pos = modelJoints[3]->actualAngle;
        res.nsKnee_pos = modelJoints[4]->actualAngle;
        res.nsAnkle_pos = modelJoints[5]->actualAngle;

        res.sAnkle_vel = modelJoints[0]->actualVelocity;
        res.sKnee_vel = modelJoints[1]->actualVelocity;
        res.sHip_vel = modelJoints[2]->actualVelocity;
        res.nsHip_vel = modelJoints[3]->actualVelocity;
        res.nsKnee_vel = modelJoints[4]->actualVelocity;
        res.nsAnkle_vel = modelJoints[5]->actualVelocity;

    }else
    {
        res.sAnkle_pos = modelJoints[5]->actualAngle;
        res.sKnee_pos = modelJoints[4]->actualAngle;
        res.sHip_pos = modelJoints[3]->actualAngle;
        res.nsHip_pos = modelJoints[2]->actualAngle;
        res.nsKnee_pos = modelJoints[1]->actualAngle;
        res.nsAnkle_pos = modelJoints[0]->actualAngle;

        res.sAnkle_vel = modelJoints[5]->actualVelocity;
        res.sKnee_vel = modelJoints[4]->actualVelocity;
        res.sHip_vel = modelJoints[3]->actualVelocity;
        res.nsHip_vel = modelJoints[2]->actualVelocity;
        res.nsKnee_vel = modelJoints[1]->actualVelocity;
        res.nsAnkle_vel = modelJoints[0]->actualVelocity;
    }

    res.Rcontact = modelContacts[0]->isContact == true ? 1:0;
    res.Lcontact = modelContacts[1]->isContact == true ? 1:0;
    return true;
}

bool ControlWrapperPlugin::commTorques(gazebowrapper::CommandTorquesRequest &req, gazebowrapper::CommandTorquesResponse &res)
{
    if(req.contactLeg == 0)
    {
        modelJoints[0]->controlSignal = req.sAnkle;
        modelJoints[1]->controlSignal = req.sKnee;
        modelJoints[2]->controlSignal = req.sHip;
        modelJoints[3]->controlSignal = req.nsHip;
        modelJoints[4]->controlSignal = req.nsKnee;
        modelJoints[5]->controlSignal = req.nsAnkle;
    }else{
        modelJoints[5]->controlSignal = req.sAnkle;
        modelJoints[4]->controlSignal = req.sKnee;
        modelJoints[3]->controlSignal = req.sHip;
        modelJoints[2]->controlSignal = req.nsHip;
        modelJoints[1]->controlSignal = req.nsKnee;
        modelJoints[0]->controlSignal = req.nsAnkle;
    }
    res.confirmation_flag = 1;

    return true;
}



