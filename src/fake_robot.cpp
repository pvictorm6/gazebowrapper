#include "fake_robot.hpp"

GzRobot::GzRobot(ros::NodeHandlePtr &nh)
{
    feedback_client = ros::ServiceClientPtr(new ros::ServiceClient);
    command_client = ros::ServiceClientPtr(new ros::ServiceClient);

    *feedback_client = nh->serviceClient<gazebowrapper::ReqFeedback>("reqFeedback");
    *command_client = nh->serviceClient<gazebowrapper::CommandTorques>("commTorques");
    actual_feedback = new double[3 * 6];
    actual_contacts = new double[2];
}

void GzRobot::spinOnce()
{
    feedback.request.flag_reference = 0;
    if(feedback_client->call(feedback))
    {
        actual_feedback[0] = feedback.response.sAnkle_pos;
        actual_feedback[1] = feedback.response.sAnkle_vel;
        actual_feedback[2] = feedback.response.sAnkle_torque;

        actual_feedback[3] = feedback.response.sKnee_pos;
        actual_feedback[4] = feedback.response.sKnee_vel;
        actual_feedback[5] = feedback.response.sKnee_torque;

        actual_feedback[6] = feedback.response.sHip_pos;
        actual_feedback[7] = feedback.response.sHip_vel;
        actual_feedback[8] = feedback.response.sHip_torque;

        actual_feedback[9] = feedback.response.nsHip_pos;
        actual_feedback[10] = feedback.response.nsHip_vel;
        actual_feedback[11] = feedback.response.nsHip_torque;

        actual_feedback[12] = feedback.response.nsKnee_pos;
        actual_feedback[13] = feedback.response.nsKnee_vel;
        actual_feedback[14] = feedback.response.nsKnee_torque;

        actual_feedback[15] = feedback.response.nsAnkle_pos;
        actual_feedback[16] = feedback.response.nsAnkle_vel;
        actual_feedback[17] = feedback.response.nsAnkle_torque;

        actual_contacts[0] = feedback.response.Rcontact;
        actual_contacts[1] = feedback.response.Lcontact;
    }else{
        ROS_ERROR("Not able to get feedback");
    }
}

void GzRobot::getFeedback(double *feedback)
{
    for(int i = 0; i < 18; ++i)
    {
        feedback[i] = actual_feedback[i];
    }
}

void GzRobot::getContacts(double *contacts)
{
    contacts[0] = actual_contacts[0];
    contacts[1] = actual_contacts[1];
}

void GzRobot::setCommand(double *command)
{
    torques.request.sAnkle = command[0];
    torques.request.sKnee = command[1];
    torques.request.sHip = command[2];
    torques.request.nsHip = command[3];
    torques.request.nsKnee = command[4];
    torques.request.nsAnkle = command[5];

    torques.request.contactLeg = 0;

    if(!command_client->call(torques))
    {
        ROS_ERROR("Not able to command torques");
    }
}
